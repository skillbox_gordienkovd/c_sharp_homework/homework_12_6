using System.Windows;
using System.Windows.Controls;
using homework_12_6.Department.Add;
using homework_12_6.Department.Details;
using homework_12_6.Department.Edit;

namespace homework_12_6.Department.List
{
    public partial class DepartmentList : Page
    {
        public DepartmentList()
        {
            InitializeComponent();
            DepartmentListBox.ItemsSource = Repository.Repository.DepartmentRepository.Departments;
            DepartmentListBox.Items.Refresh();
        }

        private void EditClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            var departmentEditPage = new DepartmentEdit(id);
            NavigationService?.Navigate(departmentEditPage);
        }

        private void DetailsClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            var departmentDetailsPage = new DepartmentDetails(id);
            NavigationService?.Navigate(departmentDetailsPage);
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            Repository.Repository.DepartmentRepository.Delete(id);
            DepartmentListBox.Items.Refresh();
        }

        private void Add(object sender, RoutedEventArgs e)
        {
            var departmentAddPage = new DepartmentAdd();
            var navigationService = NavigationService;
            navigationService?.Navigate(departmentAddPage);
        }
    }
}