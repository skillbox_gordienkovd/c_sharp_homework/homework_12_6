using System.Windows;
using System.Windows.Controls;
using homework_12_6.Department.List;

namespace homework_12_6.Department.Edit
{
    public partial class DepartmentEdit : Page
    {
        private Domain.Department _department;
        public DepartmentEdit(long id)
        {
            _department = Repository.Repository.DepartmentRepository.FindById(id);
            InitializeComponent();
            DepartmentName.Text = _department.Name;
        }

        private void Save(object sender, RoutedEventArgs routedEventArgs)
        {
            _department.Name = DepartmentName.Text;
            Repository.Repository.DepartmentRepository.Save(_department);
            var departmentList = new DepartmentList();
            NavigationService?.Navigate(departmentList);
        }
    }
}