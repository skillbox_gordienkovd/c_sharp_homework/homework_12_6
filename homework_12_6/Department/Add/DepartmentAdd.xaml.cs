using System.Windows;
using System.Windows.Controls;
using homework_12_6.Department.List;

namespace homework_12_6.Department.Add
{
    public partial class DepartmentAdd : Page
    {
        private readonly Domain.Department _department = new Domain.Department();

        public DepartmentAdd()
        {
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            _department.Name = DepartmentName.Text;
            Repository.Repository.DepartmentRepository.Save(_department);
            var departmentList = new DepartmentList();
            NavigationService?.Navigate(departmentList);
        }
    }
}