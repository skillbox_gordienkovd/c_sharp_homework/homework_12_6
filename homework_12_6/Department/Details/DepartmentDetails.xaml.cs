using System.Windows;
using System.Windows.Controls;
using homework_12_6.Employee.List;

namespace homework_12_6.Department.Details
{
    public partial class DepartmentDetails : Page
    {
        private Domain.Department _department;
        public DepartmentDetails(long id)
        {
            _department = Repository.Repository.DepartmentRepository.FindById(id);
            InitializeComponent();
            DepartmentName.Text = _department.Name;
        }

        private void ShowEmployees(object sender, RoutedEventArgs e)
        {
            var employeeListPage = new EmployeeList(_department.Id);
            NavigationService?.Navigate(employeeListPage);
        }
    }
}