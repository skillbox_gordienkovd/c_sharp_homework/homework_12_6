using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace homework_12_6.Repository
{
    public class EmployeeRepository
    {
        public ObservableCollection<Domain.Employee> Employees { get; set; } =
            new ObservableCollection<Domain.Employee>();

        public Domain.Employee Save(Domain.Employee employee)
        {
            if (employee.Id == 0)
            {
                employee.Id = Repository.SequenceRepository.EmployeeId();
                Employees.Add(employee);
                SaveDb();
            }
            else
            {
                var found = FindById(employee.Id);

                if (found != null)
                {
                    found.Firstname = employee.Firstname;
                    found.Lastname = employee.Lastname;
                    found.Age = employee.Age;
                    found.Salary = employee.Salary;
                    found.EmployeeType = employee.EmployeeType;
                    SaveDb();
                }
                else
                {
                    employee.Id = Repository.SequenceRepository.EmployeeId();
                    SaveDb();
                }
            }

            return employee;
        }

        public void Delete(long id)
        {
            Employees.Remove(FindById(id));
            SaveDb();
        }

        public Domain.Employee FindById(long id)
        {
            return Employees.Single(item => item.Id == id);
        }

        public IEnumerable<Domain.Employee> FindByDepartmentId(long departmentId)
        {
            return Employees.Where(item => item.DepartmentId == departmentId);
        }

        private void SaveDb()
        {
            File.WriteAllText("employees_db.json", JsonSerializer.Serialize(this));
        }
    }
}