using System.IO;
using System.Text.Json;

namespace homework_12_6.Repository
{
    public static class Repository
    {
        public static readonly SequenceRepository SequenceRepository = new SequenceRepository();
        public static DepartmentRepository DepartmentRepository = new DepartmentRepository();
        public static EmployeeRepository EmployeeRepository = new EmployeeRepository();


        static Repository()
        {
            InitDepartments();
            InitEmployees();
        }

        private static void InitDepartments()
        {
            if (File.Exists("departments_db.json"))
                DepartmentRepository =
                    JsonSerializer.Deserialize<DepartmentRepository>(File.ReadAllText("departments_db.json"));
        }
        
        private static void InitEmployees()
        {
            if (File.Exists("employees_db.json"))
                EmployeeRepository =
                    JsonSerializer.Deserialize<EmployeeRepository>(File.ReadAllText("employees_db.json"));
        }
    }
}