using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace homework_12_6.Repository
{
    public class DepartmentRepository
    {
        public ObservableCollection<Domain.Department> Departments { get; set; } = new ObservableCollection<Domain.Department>();

        public Domain.Department Save(Domain.Department department)
        {
            if (department.Id == 0)
            {
                department.Id = Repository.SequenceRepository.DepartmentId();
                Departments.Add(department);
                SaveDb();
            }
            else
            {
                var found = FindById(department.Id);

                if (found != null)
                {
                    found.Name = department.Name;
                    SaveDb();
                }
                else
                {
                    department.Id = Repository.SequenceRepository.DepartmentId();
                    SaveDb();
                }
            }

            return department;
        }

        public void Delete(long id)
        {
            Departments.Remove(FindById(id));
            SaveDb();
        }

        public Domain.Department FindById(long id)
        {
            return Departments.Single(item => item.Id == id);
        }

        public IEnumerable<Domain.Department> FindAll()
        {
            return Departments;
        }

        private void SaveDb()
        {
            File.WriteAllText("departments_db.json", JsonSerializer.Serialize(this));
        }
    }
}