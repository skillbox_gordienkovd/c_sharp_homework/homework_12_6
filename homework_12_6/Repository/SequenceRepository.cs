using System.IO;
using System.Text.Json;
using homework_12_6.Domain;

namespace homework_12_6.Repository
{
    public class SequenceRepository
    {
        private Sequence _sequence = new Sequence();

        public SequenceRepository()
        {
            if (File.Exists("sequence_db.json"))
            {
                _sequence = JsonSerializer.Deserialize<Sequence>(File.ReadAllText("sequence_db.json"));
            }
        }

        public long DepartmentId()
        {
            var id = _sequence.SequenceDepartmentId();
            Save();
            return id;
        }
        
        public long EmployeeId()
        {
            return _sequence.SequenceEmployeeId();
        }

        private void Save()
        {
            File.WriteAllText("sequence_db.json", JsonSerializer.Serialize(_sequence));
        }
    }
}