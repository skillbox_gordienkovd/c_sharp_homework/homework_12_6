using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using homework_12_6.Employee.Add;
using homework_12_6.Employee.Details;
using homework_12_6.Employee.Edit;

namespace homework_12_6.Employee.List
{
    public partial class EmployeeList : Page
    {
        private readonly long _departmentId;
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;
        public EmployeeList()
        {
        }

        public EmployeeList(long id)
        {
            _departmentId = id;
            InitializeComponent();
            EmployeeListBox.ItemsSource = Repository.Repository.EmployeeRepository.FindByDepartmentId(id);
            EmployeeListBox.Items.Refresh();
        }

        private void EditClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            var employeeEditPage = new EmployeeEdit(id);
            NavigationService?.Navigate(employeeEditPage);
        }

        private void DetailsClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            var employeeDetailsPage = new EmployeeDetails(id);
            NavigationService?.Navigate(employeeDetailsPage);
        }

        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            var id = (long) ((Button) sender).Tag;
            Repository.Repository.EmployeeRepository.Delete(id);
            EmployeeListBox.ItemsSource = Repository.Repository.EmployeeRepository.FindByDepartmentId(id);
            EmployeeListBox.Items.Refresh();
        }

        private void Add(object sender, RoutedEventArgs e)
        {
            var employeeAddPage = new EmployeeAdd(_departmentId);
            var navigationService = NavigationService;
            navigationService?.Navigate(employeeAddPage);
        }

        private void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {
            var headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked == null) return;
            if (headerClicked.Role == GridViewColumnHeaderRole.Padding) return;
            if (headerClicked != _lastHeaderClicked)
            {
                direction = ListSortDirection.Ascending;
                        
            }
            else
            {
                if (_lastDirection == ListSortDirection.Ascending)
                {
                    direction = ListSortDirection.Descending;
                }
                else
                {
                    direction = ListSortDirection.Ascending;
                }
            }
            var columnBinding = headerClicked.Column.DisplayMemberBinding as Binding;
            var sortBy = columnBinding?.Path.Path ?? headerClicked.Column.Header as string;
            if (sortBy == "Age" || sortBy == "Salary")
            {
                EmployeeListBox.Items.SortDescriptions.Clear();
                var sd = new SortDescription(sortBy, direction);
                EmployeeListBox.Items.SortDescriptions.Add(sd);
                EmployeeListBox.Items.Refresh();
            }

            _lastHeaderClicked = headerClicked;
            _lastDirection = direction;
        }
    }
}