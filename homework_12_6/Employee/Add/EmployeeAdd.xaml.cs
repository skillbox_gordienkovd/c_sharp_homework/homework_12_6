using System.Windows;
using System.Windows.Controls;
using homework_12_6.Department.Details;
using homework_12_6.Domain.Enumeration;

namespace homework_12_6.Employee.Add
{
    public partial class EmployeeAdd : Page
    {
        public Domain.Employee _employee = new Domain.Employee();
        public EmployeeAdd(long departmentId)
        {
            _employee.DepartmentId = departmentId;
            InitializeComponent();
            DepartmentId.Text = departmentId.ToString();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            _employee.Firstname = EmployeeFirstName.Text;
            _employee.Lastname = EmployeeLastName.Text;
            uint.TryParse(EmployeeAge.Text, out uint age);
            _employee.Age = age;
            uint.TryParse(EmployeeSalary.Text, out uint salary);
            _employee.Salary = salary;
            _employee.EmployeeType = (EmployeeType)EmployeeType.SelectedItem;
            Repository.Repository.EmployeeRepository.Save(_employee);
            NavigationService?.Navigate(new DepartmentDetails(_employee.DepartmentId));
        }
    }
}