using System.Windows.Controls;

namespace homework_12_6.Employee.Details
{
    public partial class EmployeeDetails : Page
    {
        private Domain.Employee _employee;
        public EmployeeDetails(long id)
        {
            _employee = Repository.Repository.EmployeeRepository.FindById(id);
            InitializeComponent();
            EmployeeFirstName.Text = _employee.Firstname;
            EmployeeLastName.Text = _employee.Lastname;
            EmployeeAge.Text = _employee.Age.ToString();
            EmployeeSalary.Text = _employee.Salary.ToString();
            EmployeeType.Text = _employee.EmployeeType.ToString();
            DepartmentId.Text = _employee.DepartmentId.ToString();
        }
    }
}