using System.Windows;
using System.Windows.Controls;
using homework_12_6.Department.Details;
using homework_12_6.Domain.Enumeration;

namespace homework_12_6.Employee.Edit
{
    public partial class EmployeeEdit : Page
    {
        private readonly Domain.Employee _employee;
        public EmployeeEdit(long id)
        {
            _employee = Repository.Repository.EmployeeRepository.FindById(id);
            InitializeComponent();
            EmployeeFirstName.Text = _employee.Firstname;
            EmployeeLastName.Text = _employee.Lastname;
            EmployeeAge.Text = _employee.Age.ToString();
            EmployeeSalary.Text = _employee.Salary.ToString();
            EmployeeType.SelectedItem = _employee.EmployeeType;
            DepartmentId.Text = _employee.DepartmentId.ToString();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            _employee.Firstname = EmployeeFirstName.Text;
            _employee.Lastname = EmployeeLastName.Text;
            uint.TryParse(EmployeeAge.Text, out uint age);
            _employee.Age = age;
            uint.TryParse(EmployeeSalary.Text, out uint salary);
            _employee.Salary = salary;
            _employee.EmployeeType = (EmployeeType)EmployeeType.SelectedItem;
            Repository.Repository.EmployeeRepository.Save(_employee);
            NavigationService?.Navigate(new DepartmentDetails(_employee.DepartmentId));
        }
    }
}