using homework_12_6.Domain.Enumeration;

namespace homework_12_6.Domain
{
    public class Employee
    {
        public long Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public uint Age { get; set; }
        public uint Salary { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public long DepartmentId { get; set; }
    }
}