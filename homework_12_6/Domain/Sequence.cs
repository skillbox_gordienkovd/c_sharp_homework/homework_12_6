namespace homework_12_6.Domain
{
    public class Sequence
    {
        public long DepartmentId { get; set; }

        public long EmployeeId { get; set; }

        public long SequenceEmployeeId()
        {
            return ++EmployeeId;
        }

        public long SequenceDepartmentId()
        {
            return ++DepartmentId;
        }
    }
}