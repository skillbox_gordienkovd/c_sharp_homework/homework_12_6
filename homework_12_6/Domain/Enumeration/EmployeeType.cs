namespace homework_12_6.Domain.Enumeration
{
    public enum EmployeeType
    {
       Director,
       Worker,
       Intern
    }
}