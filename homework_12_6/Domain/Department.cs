namespace homework_12_6.Domain
{
    public class Department
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}