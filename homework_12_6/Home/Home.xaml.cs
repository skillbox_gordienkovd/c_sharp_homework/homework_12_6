using System.Windows.Controls;
using System.Windows.Input;
using homework_12_6.Department.List;
using homework_12_6.Employee.List;

namespace homework_12_6.Home
{
    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();
        }

        private void NavigateDepartmentList(object sender, MouseButtonEventArgs e)
        {
            var departmentListPage = new DepartmentList();
            var navigationService = NavigationService;
            navigationService?.Navigate(departmentListPage);
        }
    }
}